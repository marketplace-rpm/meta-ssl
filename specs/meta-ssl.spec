%global d_bin                   %{_bindir}

Name:                           meta-ssl
Version:                        1.0.0
Release:                        1%{?dist}
Summary:                        META-package for install SSL
License:                        GPLv3

Source10:                       app.ssl.cert.sh

Requires:                       bash zsh openssl

%description
META-package for install SSL.

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

%prep


%install
%{__rm} -rf %{buildroot}

%{__install} -Dp -m 0755 %{SOURCE10} \
  %{buildroot}%{d_bin}/app.acme.sh


%files
%{d_bin}/app.acme.sh


%changelog
* Sat Oct 19 2019 MARKETPLACE <uid.marketplace@gmail.com> - 1.0.0-1
- Initial build.
