#!/usr/bin/env bash

(( ${EUID} == 0 )) &&
  { echo >&2 "This script should not be run as root!"; exit 1; }

# -------------------------------------------------------------------------------------------------------------------- #
# Get options.
# -------------------------------------------------------------------------------------------------------------------- #

OPTIND=1

while getopts "k:c:d:s:h" opt; do
  case ${opt} in
    k)
      ca_key="${OPTARG}"
      ;;
    c)
      ca_crt="${OPTARG}"
      ;;
    d)
      ssl_days="${OPTARG}"
      ;;
    s)
      ssl_cnf="${OPTARG}"
      ;;
    h|*)
      echo "-k [CA KEY path] | -c [CA CRT path] | -d [days] | -s [openssl.cnf path]"
      exit 2
      ;;
    \?)
      echo "Invalid option: -${OPTARG}."
      exit 1
      ;;
    :)
      echo "Option -${OPTARG} requires an argument."
      exit 1
      ;;
  esac
done

shift $(( ${OPTIND} - 1 ))

[[ -z "${ca_key}" ]] || [[ -z "${ca_crt}" ]] && exit 1

# -------------------------------------------------------------------------------------------------------------------- #
# -----------------------------------------------------< SCRIPT >----------------------------------------------------- #
# -------------------------------------------------------------------------------------------------------------------- #

id="$(date +%s%N | sha512sum | fold -w 8 | head -n 1)"
sha="sha256"
curve="secp384r1"

if [[ -z "${ssl_cnf}" ]]; then
  config=""
else
  config="-config ${ssl_cnf}"
fi

if [[ -z "${ssl_days}" ]]; then
  days="3650"
else
  days="${ssl_days}"
fi

ee_key="ee.${id}.key"
ee_crt="ee.${id}.crt"
ee_csr="ee.${id}.csr"

[[ ! -f "${ca_key}" ]] || [[ ! -f "${ca_crt}" ]] &&
  { echo >&2 "Not found: CA CRT or CA KEY "; exit 1; }

# Generate EC private key for EE (End Entity).
echo "--- Generate EE (End Entity): KEY"
openssl genpkey -algorithm ec -out ${ee_key}  \
  -pkeyopt ec_paramgen_curve:${curve}         \
  -pkeyopt ec_param_enc:named_curve
echo "--- Done" && echo ""

# Generate certificate signing request for ECDSA EE (End Entity).
echo "--- Generate EE (End Entity): CSR"
openssl req -new -${sha} ${config}  \
  -key ${ee_key}                    \
  -out ${ee_csr}
echo "--- Done" && echo ""

# Generate ECDSA EE (End Entity) based on the above CSR, and sign it with the above ECDSA CA.
echo "--- Generate EE (End Entity): CRT"
openssl x509 -req -days ${days} -${sha} \
  -CAcreateserial                       \
  -CA ${ca_crt}                         \
  -CAkey ${ca_key}                      \
  -in ${ee_csr}                         \
  -out ${ee_crt}
echo "--- Done" && echo ""

# -------------------------------------------------------------------------------------------------------------------- #
# Exit.
# -------------------------------------------------------------------------------------------------------------------- #

exit 0
